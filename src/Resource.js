var s_music_background = 'effects/DogBark.mp3';
var dog2suf = 'effects/dog2suf.mp3';
var dog2dead = 'effects/dog2dead.mp3';
var dog1suf = 'effects/dog1suf.mp3';
var dog1dead = 'effects/dog2dead.mp3';
var song = 'effects/newreligion.mp3';
var hit = 'effects/GlassBreak.mp3';
var whistle = 'effects/Whistle.mp3';
var dbdskill = 'effects/dbdskill.mp3';
var speedskill = 'effects/speedskill.mp3';
var healskill = 'effects/healskill.mp3';
var dogBG = 'effects/dogBG.mp3';
var click = 'effects/click.mp3';
var g_resources = [
    //image
    {src: s_music_background },
	{src: dog2suf },
    {src: dog2dead },
 	{src: dog1suf },
 	{src: dog1dead },
 	{src: song },
 	{src: hit},
 	{src: whistle},
    {src: dbdskill},
    {src: speedskill},
    {src: healskill},
    {src: dogBG},
    {src: click}
    
    //plist
 
    //fnt
 
    //tmx
 
    //bgm
 
    //effect
];