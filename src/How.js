var How = cc.Layer.extend({
    ctor: function(select){
        this._super();
        this.select = select;
    },
    init: function() {
		this.BackGround = new BackGround('images/HTP2.png' );
        this.BackGround.setPosition( new cc.Point( 497.5, 300 ) );
        this.addChild( this.BackGround );
 		this.setKeyboardEnabled( true );
 		},

 	onKeyDown: function( e ) {
 		var clicks = cc.AudioEngine.getInstance();
        clicks.preloadMusic(click);
        if(e == 13){
 			clicks.playEffect( 'effects/click.mp3' );
 			var director = cc.Director.getInstance();
        	director.replaceScene(cc.TransitionFade.create(1.5, new Startscene(this.select)));
 		}
    }
   });
    var Hows = cc.Scene.extend({
    ctor: function(select){
        this._super();
        this.select = select;
    },
    onEnter: function() {
        this._super();
        var layer = new How(this.select);
        layer.init();
        this.addChild( layer );
    }
    

});