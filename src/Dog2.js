var Dog2 = cc.Sprite.extend({
    ctor: function(select) {
        this._super();
        this.initWithFile( 'images/dog2.png' );
         var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'images/dog2runleft1.png' );
        animation.addSpriteFrameWithFile( 'images/dog2runleft2.png' );
        animation.setDelayPerUnit( 0.2 );
        var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
        this.runAction( movingAction );
        this.alive = true;
        this.vx = 3;
        this.AIBOT = select;

        this.AIrunperiod = Math.floor(Math.random() * (100 - 30 + 30)) + 30;
        this.direction = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
    },
    update: function( dt ) {
      if(this.AIBOT == false){
      var pos = this.getPosition();
        if(this.directionz == Dog1.DIR.LEFT && this.alive == true){
          if( pos.x < (screenWidth/2) + 50 ) {
            this.setPosition( new cc.Point( pos.x + 1 , pos.y  ) );
          }else{
            this.setPosition( new cc.Point( pos.x - this.vx, pos.y ) );
          }
        }
      else if (this.directionz == Dog1.DIR.RIGHT && this.alive == true){
        if( pos.x > screenWidth  ) {
          this.setPosition( new cc.Point( pos.x - 1  , pos.y ) );
        }else{
          this.setPosition( new cc.Point( pos.x + this.vx , pos.y ) );
        }
      }
      }else{
      var pos = this.getPosition();
        if(this.direction == 1 && this.alive == true ){
          this.AIrunperiod -= 1; 
          if( pos.x < (screenWidth/2) + 50 ) {
              this.setPosition( new cc.Point( pos.x + 1 , pos.y  ) );
          }else{
              this.setPosition( new cc.Point( pos.x - this.vx, pos.y ) );
          }
          if(this.AIrunperiod <= 0){
            this.direction = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
            this.AIrunperiod = Math.floor(Math.random() * (100 - 30 + 30)) + 30;
          }
        }else if(this.direction == 2 && this.alive == true ){
          this.AIrunperiod -= 1; 
          if( pos.x > screenWidth  ) {
            this.setPosition( new cc.Point( pos.x - 1  , pos.y ) );
          }else{
            this.setPosition( new cc.Point( pos.x + this.vx , pos.y ) );
          }
          if(this.AIrunperiod <= 0){
              this.direction = Math.floor(Math.random() * (2 - 1 + 1)) + 1;
              this.AIrunperiod = Math.floor(Math.random() * (100 - 30 + 30)) + 30;
              }
          }
        }
    },
    switchDirection: function(direction) {
      if( direction == "RIGHT" ) {
        this.directionz = Dog2.DIR.RIGHT;
      }else if( direction == "LEFT"){
        this.directionz = Dog2.DIR.LEFT; 
    }
    },
    switchDirectionSTILL: function() {
      this.direction = Dog2.DIR.STILL;
      }
});
Dog2.DIR = {
    LEFT: 1,
    RIGHT: 2
};