 var itemdog1 = cc.Sprite.extend({
    ctor: function( skill ) {
        this._super();
        this.initWithFile( 'images/SynysterGates.png' );
        this.direction = itemdog1.DIR.UNSHOT;
        this.vx = 0;
        this.vy = 0;
        this.Shooting = false; 
        this.Stun = false;
        this.health = 6;
        this.timeout = false;
        this.win = false;
        this.dbdskilltime = 1;
        this.speedskilltime = 2;
        this.healskilltime = 2;
        this.dbdskill = cc.AudioEngine.getInstance();
        this.dbdskill.preloadMusic(dbdskill);
        this.speedskill = cc.AudioEngine.getInstance();
        this.speedskill.preloadMusic(speedskill);
        this.healskill = cc.AudioEngine.getInstance();
        this.healskill.preloadMusic(healskill);
        this.Skill = skill;
        this.onetimehitbug = true;
        this.speedperiod = 300;
        this.usespeed = false;
        this.dog2effect = cc.AudioEngine.getInstance();
    },
    update: function( dt ) {
        var pos = this.getPosition();
          if(this.direction == itemdog1.DIR.UNSHOT){
                this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ) );
          }
          else if(this.direction == itemdog1.DIR.SHOT && this.dog.alive == true ){
                var pos = this.getPosition();   
                this.setPosition( new cc.Point( pos.x + this.vx, pos.y + this.vy ) );
                this.Shooting = true; 
                    if(pos.y > 0 ){
                        this.vy += -1;
                    }
                    if((pos.x > 390 && pos.x < 440 )&& (pos.y < 220)){
                        this.vx = 0;
                        this.vy += -2;
                    }
        }
        this.hitDog();
        if(pos.x > screenWidth){
                this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ));
                this.vx = 0;
                this.vy = 0;
                this.direction = itemdog1.DIR.UNSHOT;
                this.Shooting = false ;
                this.onetimehitbug = true;
        }
        if(pos.y < 0){
                this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ));
                this.vx = 0;
                this.vy = 0;
                this.direction = itemdog1.DIR.UNSHOT;
                this.Shooting = false;
                this.onetimehitbug = true;
        }
        if(this.usespeed == true){
            this.speedperiod -= 1;
            if( this.speedperiod <= 0){
                this.dog.vx -= 6;
                this.speedskilltime -= 1;
                this.usespeed = false;
                this.speedperiod = 300;
             }
         }
    },
    setDog: function(dog){
          this.dog = dog;
    },
    setDog2: function(dog){
          this.dog2 = dog;
    },
    setItemDog2: function(itemdog2){
          this.itemdog2 = itemdog2;
    },
    setBarhealth1: function(Barhealth){
          this.barhealth1 = Barhealth;
    },
    setBarhealth2: function(Barhealth){
          this.barhealth2 = Barhealth;
    },
    switchDirection: function(direction) {
      if(direction == "UNSHOT") {
          this.direction = itemdog1.DIR.UNSHOT;
      }else if( direction == "SHOT"){
          this.direction = itemdog1.DIR.SHOT; 
      }
    },
    hitDog: function(){
        var pos = this.getPosition();   
        var dog2effect = cc.AudioEngine.getInstance();
        this.dog2effect.preloadMusic(dog2suf);
        this.dog2effect.preloadMusic(song);
        this.dog2effect.preloadMusic(hit);
        if( this.timeout == false){
            if(pos.x > this.dog2.getPosition().x - 20 && pos.x < this.dog2.getPosition().x + 55 && pos.y > this.dog2.getPosition().y - 20 && pos.y < this.dog2.getPosition().y + 40  && this.onetimehitbug == true){
                //this.Stun == true;
                this.health -= 1;
                this.onetimehitbug = false;
                this.dog2.initWithFile( 'images/dog2runleft1stun.png');
                if(this.health == 5){
                    this.barhealth2.initWithFile( 'images/barhealth1down1.png');
                    dog2effect.playEffect( 'effects/dog2suf.mp3' );
                    dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                }
                if(this.health == 4){
                    this.barhealth2.initWithFile( 'images/barhealth1down2.png');
                    this.dog2effect.playEffect( 'effects/dog2suf.mp3' );
                    this.dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                }
                if(this.health == 3){
                    this.barhealth2.initWithFile( 'images/barhealth1down3.png');
                    this.dog2effect.playEffect( 'effects/dog2suf.mp3' );
                    this.dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                }
                if(this.health == 2){
                    this.barhealth2.initWithFile( 'images/barhealth1down4.png');
                    this.dog2effect.playEffect( 'effects/dog2suf.mp3' );
                    this.dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                }
                if(this.health == 1){
                    this.barhealth2.initWithFile( 'images/barhealth1down5.png');
                    this.dog2effect.playEffect( 'effects/dog2suf.mp3' );
                    this.dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                }
                if(this.health == 0){
                    this.barhealth2.initWithFile( 'images/barhealth1down6.png');
                    this.dog2effect.playEffect( 'effects/dog2dead.mp3' );
                    this.dog2effect.playEffect( 'effects/GlassBreak.mp3' );
                    this.dog2effect.playEffect( 'effects/newreligion.mp3' );
                    this.itemdog2.setPosition(new cc.Point( this.dog2.getPosition.x, this.dog2.getPosition.y));
                }
            }
            if(this.health <= 0){
                this.dog2.initWithFile( 'images/skull3.png'); 
                this.itemdog2.initWithFile( 'images/empty.png');
                this.dog2.alive = false; 
                this.win = true;  
            }
        }
    },
    SkillItem: function(numberskill){
      var doubledamageskill = 1;
      var speedskill = 2;
      var healskill = 3;
      if(doubledamageskill  == numberskill && this.dbdskilltime > 0){
          this.dog.initWithFile( 'images/dog1cast.png');
          this.dbdskill.playEffect( 'effects/dbdskill.mp3' );
          if(this.health >1){
              this.health -= 1;
          }
          this.dbdskilltime -= 1;
          if(this.dbdskilltime == 0){
              this.Skill[0].removeFromParent();
          }
      }
      if(speedskill == numberskill &&  this.usespeed == false){
          if(this.speedskilltime > 0){
              this.dog.initWithFile( 'images/speedskill.png');
              this.speedskill.playEffect( 'effects/speedskill.mp3' );
              this.dog.vx += 6;
              this.usespeed = true;
              if(this.speedskilltime == 1){
                  this.Skill[1].removeFromParent();
              }
          }
      }
      if(healskill == numberskill && this.healskilltime > 0){
            if(this.itemdog2.health == 5){
                this.barhealth1.initWithFile( 'images/barhealth1.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.dog.initWithFile( 'images/d1heal.png');
                this.itemdog2.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[2].removeFromParent();
                }
            }
            if(this.itemdog2.health == 4){
                this.barhealth1.initWithFile( 'images/barhealth2down1.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.dog.initWithFile( 'images/d1heal.png');
                this.itemdog2.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[2].removeFromParent();
                }
            }
            if(this.itemdog2.health == 3){
                this.barhealth1.initWithFile( 'images/barhealth2down2.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.dog.initWithFile( 'images/d1heal.png');
                this.itemdog2.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[2].removeFromParent();
                }
            }
            if(this.itemdog2.health == 2){
                this.barhealth1.initWithFile( 'images/barhealth2down3.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.dog.initWithFile( 'images/d1heal.png');
                this.itemdog2.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[2].removeFromParent();
                }
            }
            if(this.itemdog2.health == 1){
                this.barhealth1.initWithFile( 'images/barhealth2down4.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.dog.initWithFile( 'images/d1heal.png');
                this.itemdog2.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[2].removeFromParent();
                }
            }
        }
    },
    shot: function(degree){
        if(degree == "45"){
            if(this.Shooting == false){
                this.vx = 10;
                this.vy = 20;
            }else{
                this.vx += 0;
                this.vy += 0;
            }
        }else if(degree == "30"){
            if(this.Shooting == false){
                this.vx = 18;
                this.vy = 20;
            }else{
                this.vx += 0;
                this.vy += 0;
            }
        }else if(degree == "60"){
            if(this.Shooting == false){
                this.vx = 5;
                this.vy = 25;
            }else{
                this.vx += 0;
                this.vy += 0;
            }
        }
    },
    switchDirectionSTILL: function() {
        this.direction = itemdog1.DIR.STILL;
    }
});
itemdog1.G = -1;
itemdog1.STARTING_VELOCITY = 15;
itemdog1.DIR = {
    LEFT: 1,
    RIGHT: 2,
    SHOT: 3
};