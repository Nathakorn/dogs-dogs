var select = cc.Layer.extend({
    init: function() {
        this.BackGround = new BackGround('images/modeselect.png' );
        this.BackGround.setPosition( new cc.Point( 497.5, 300 ) );
        this.addChild( this.BackGround );
        this.setKeyboardEnabled( true );
    },
    onKeyDown: function( e ) {
        var clicks = cc.AudioEngine.getInstance();
        clicks.preloadMusic(click);
        if(e == cc.KEY.s){
            clicks.playEffect( 'effects/click.mp3' );
            var director = cc.Director.getInstance();
            director.replaceScene(cc.TransitionFade.create(1.5, new Hows(true)));
        }else{
            if(e == cc.KEY.m){
                clicks.playEffect( 'effects/click.mp3' );
                var director = cc.Director.getInstance();
                director.replaceScene(cc.TransitionFade.create(1.5, new Hows(false)));
            }
        }
    }
    });
    var selects = cc.Scene.extend({
    onEnter: function(){
        this._super();
        var layer = new select();
        layer.init();
        this.addChild( layer );
    }
    

});