var itemdog2 = cc.Sprite.extend({
    ctor: function(skill) {
        this._super();
        this.initWithFile( 'images/Krome.png' );
        this.direction = itemdog2.DIR.UNSHOT; 
        this.vx = 0;
        this.vy = 0;
        this.net = Net;
        this.Shooting = false;
        this.health = 6;
        this.timeout = false; 
        this.win = false;
        this.dbdskilltime = 1;
        this.speedskilltime = 2;
        this.healskilltime = 2;
        this.dbdskill = cc.AudioEngine.getInstance();
        this.dbdskill.preloadMusic(dbdskill);
        this.speedskill = cc.AudioEngine.getInstance();
        this.speedskill.preloadMusic(speedskill);
        this.healskill = cc.AudioEngine.getInstance();
        this.healskill.preloadMusic(healskill);
        this.speedskillperiod = 2;
        this.Skill = skill;
        this.onetimehitbug = true;
        this.speedperiod = 300;
        this.usespeed = false;
        this.dog1effect = cc.AudioEngine.getInstance();
        this.degree = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
    },
    update: function( dt ) {
      var pos = this.getPosition();
      if(this.dog.AIBOT == true){
          this.SkillItem(4);
          this.shot(this.degree);
          this.direction = itemdog2.DIR.SHOT;
      }
      if(this.direction == itemdog2.DIR.UNSHOT){
          this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ) );
      }else if(this.direction == itemdog2.DIR.SHOT && this.dog.alive == true ){
          this.Shooting = true; 
          this.setPosition( new cc.Point( pos.x - this.vx, pos.y + this.vy ) );
            if(pos.y > 0 ) {
                this.vy += -1;
            }
            if((pos.x > 390 && pos.x < 415 )&& (pos.y < 220)){
                this.vx = 0;
                this.vy += -2;
            }
      }
      this.hitDog();
      if(pos.y < 0){
          this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ));
          this.vx = 0;
          this.vy = 0;
          this.direction = itemdog1.DIR.UNSHOT;
          this.Shooting = false;
          this.onetimehitbug = true;
      }
      if(pos.x < 0){
          this.setPosition( new cc.Point( this.dog.getPosition().x , this.dog.getPosition().y ));
          this.vx = 0;
          this.vy = 0;
          this.direction = itemdog1.DIR.UNSHOT;
          this.Shooting = false;
          this.onetimehitbug = true;
      }
      if(this.usespeed == true){
          this.speedperiod -= 1;
          if( this.speedperiod <= 0){
              this.dog.vx -= 6;
              this.speedskilltime -= 1;
              this.usespeed = false;
              this.speedperiod = 300;
            }
      }
    },
    setDog: function( dog){
        this.dog = dog;
    },
    setDog1: function( dog){
        this.dog1 = dog;
    },
    setItemDog1: function(itemdog1){
        this.itemdog1 = itemdog1;
    },
    setBarhealth1: function(Barhealth){
        this.barhealth1 = Barhealth;
    },
    setBarhealth2: function(Barhealth){
        this.barhealth2 = Barhealth;
    },
    switchDirection: function(direction) {
      if( direction == "RIGHT" ) {
          this.direction = itemdog1.DIR.UNSHOT;
      }else if( direction == "SHOT"){
          this.direction = itemdog1.DIR.SHOT; 
      }
    },
    hitDog: function(){
      var pos = this.getPosition();   
      this.dog1effect.preloadMusic(dog2suf);
      this.dog1effect.preloadMusic(song);
      if(this.timeout == false){
          if(pos.x > this.dog1.getPosition().x - 20 && pos.x < this.dog1.getPosition().x +40 && pos.y > this.dog1.getPosition().y - 20 && pos.y < this.dog1.getPosition().y + 30 && this.onetimehitbug == true){
              //this.Stun == true;
              this.health -= 1;
              this.onetimehitbug = false;
              this.dog1.initWithFile( 'images/dog1runright1stun1.png');
              if(this.health == 5){
                  this.barhealth1.initWithFile( 'images/barhealth2down1.png');
                  this.dog1effect.playEffect( 'effects/dog2suf.mp3' ); 
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' );  
              }
              if(this.health == 4){
                  this.barhealth1.initWithFile( 'images/barhealth2down2.png');
                  this.dog1effect.playEffect( 'effects/dog2suf.mp3' ); 
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' ); 
              }
              if(this.health == 3){
                  this.barhealth1.initWithFile( 'images/barhealth2down3.png');
                  this.dog1effect.playEffect( 'effects/dog2suf.mp3' ); 
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' ); 
              }
              if(this.health == 2){
                  this.barhealth1.initWithFile( 'images/barhealth2down4.png');
                  this.dog1effect.playEffect( 'effects/dog2suf.mp3' ); 
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' ); 
              }
              if(this.health == 1){
                  this.barhealth1.initWithFile( 'images/barhealth2down5.png');
                  this.dog1effect.playEffect( 'effects/dog2suf.mp3' ); 
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' ); 
              }
              if(this.health == 0){
                  this.barhealth1.initWithFile( 'images/barhealth2down6.png');
                  this.dog1effect.playEffect( 'effects/newreligion.mp3' );
                  this.dog1effect.playEffect( 'effects/GlassBreak.mp3' );
                  this.dog1effect.playEffect( 'effects/dog2dead.mp3' ); 
                  this.itemdog1.setPosition(new cc.Point( this.dog1.getPosition.x, this.dog1.getPosition.y));
              }
          }
          if(this.health <= 0){
              this.dog1.initWithFile( 'images/skull4.png');
              this.itemdog1.initWithFile( 'images/empty.png');
              this.itemdog1.setPosition(new cc.Point( 110 ,75 ));
              this.dog1.alive = false;  
              this.win = true; 
        }
    }
    },
    SkillItem: function(numberskill){
        var doubledamageskill = 1;
        var speedskill = 2;
        var healskill = 3;
     
      if(this.dog.AIBOT == false){
          if(doubledamageskill  == numberskill && this.dbdskilltime > 0){
              this.dog.initWithFile( 'images/d2cast.png');
              this.dbdskill.playEffect( 'effects/dbdskill.mp3' );
              if(this.health >1){
                  this.health -= 1;
              }
                 this.dbdskilltime -= 1;
          if(this.dbdskilltime == 0){
              this.Skill[3].removeFromParent();
          }
      }
      if(speedskill == numberskill &&  this.usespeed == false){
          if(this.speedskilltime > 0){
                this.dog.initWithFile( 'images/d2speed.png');
                this.speedskill.playEffect( 'effects/speedskill.mp3' );
                this.dog.vx += 6;
                this.usespeed = true;
                if(this.speedskilltime == 1){
                    this.Skill[4].removeFromParent();
                }
            }
        }
        if(healskill == numberskill && this.healskilltime > 0){
            if(this.itemdog1.health == 5){
                this.barhealth2.initWithFile( 'images/barhealth2.png');
                this.dog.initWithFile( 'images/d2heal.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.itemdog1.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                this.Skill[5].removeFromParent();
            }
          }
          if(this.itemdog1.health == 4){
                this.barhealth2.initWithFile( 'images/barhealth1down1.png');
                this.dog.initWithFile( 'images/d2heal.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.itemdog1.health++;
                this.healskilltime--;
            if(this.healskilltime == 0){
                this.Skill[5].removeFromParent();
            }
          }
          if(this.itemdog1.health == 3){
                this.barhealth2.initWithFile( 'images/barhealth1down2.png');
                this.dog.initWithFile( 'images/d2heal.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.itemdog1.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[5].removeFromParent();
            }
        }
          if(this.itemdog1.health == 2){
                this.barhealth2.initWithFile( 'images/barhealth1down3.png');
                this.dog.initWithFile( 'images/d2heal.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.itemdog1.health++;
                this.healskilltime--;
            if(this.healskilltime == 0){
                this.Skill[5].removeFromParent();
            }
        }
         if(this.itemdog1.health == 1){
                this.barhealth2.initWithFile( 'images/barhealth1down4.png');
                this.dog.initWithFile( 'images/d2heal.png');
                this.healskill.playEffect( 'effects/healskill.mp3' );
                this.itemdog1.health++;
                this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[5].removeFromParent();
                }
          }
        }
      }else{
          numberskill = Math.floor(Math.random() * (300 - 1 + 1)) + 1;
          if(doubledamageskill  == numberskill && this.dbdskilltime > 0){
              this.dog.initWithFile( 'images/d2cast.png');
              this.dbdskill.playEffect( 'effects/dbdskill.mp3' );
          if(this.health >1){
              this.health -= 1;
          }
              this.dbdskilltime -= 1;
          if(this.dbdskilltime == 0){
              this.Skill[3].removeFromParent();
          }
      }
      if(speedskill == numberskill &&  this.usespeed == false){
          if(this.speedskilltime > 0){
              this.dog.initWithFile( 'images/d2speed.png');
              this.speedskill.playEffect( 'effects/speedskill.mp3' );
              this.dog.vx += 6;
              this.usespeed = true;
              if(this.speedskilltime == 1){
                    this.Skill[4].removeFromParent();
              }
        
            }
        }
        if(healskill == numberskill && this.healskilltime > 0){
           if(this.itemdog1.health == 5){
              this.barhealth2.initWithFile( 'images/barhealth2.png');
              this.dog.initWithFile( 'images/d2heal.png');
              this.healskill.playEffect( 'effects/healskill.mp3' );
              this.itemdog1.health++;
              this.healskilltime--;
                if(this.healskilltime == 0){
                    this.Skill[5].removeFromParent();
                }
          }
          if(this.itemdog1.health == 4){
              this.barhealth2.initWithFile( 'images/barhealth1down1.png');
              this.dog.initWithFile( 'images/d2heal.png');
              this.healskill.playEffect( 'effects/healskill.mp3' );
              this.itemdog1.health++;
              this.healskilltime--;
              if(this.healskilltime == 0){
                  this.Skill[5].removeFromParent();
            }
          }
          if(this.itemdog1.health == 3){
              this.barhealth2.initWithFile( 'images/barhealth1down2.png');
              this.dog.initWithFile( 'images/d2heal.png');
              this.healskill.playEffect( 'effects/healskill.mp3' );
              this.itemdog1.health++;
              this.healskilltime--;
              if(this.healskilltime == 0){
                  this.Skill[5].removeFromParent();
            }
          }
          if(this.itemdog1.health == 2){
              this.barhealth2.initWithFile( 'images/barhealth1down3.png');
              this.dog.initWithFile( 'images/d2heal.png');
              this.healskill.playEffect( 'effects/healskill.mp3' );
              this.itemdog1.health++;
              this.healskilltime--;
              if(this.healskilltime == 0){
                  this.Skill[5].removeFromParent();
              }
          }
          if(this.itemdog1.health == 1){
              this.barhealth2.initWithFile( 'images/barhealth1down4.png');
              this.dog.initWithFile( 'images/d2heal.png');
              this.healskill.playEffect( 'effects/healskill.mp3' );
              this.itemdog1.health++;
              this.healskilltime--;
              if(this.healskilltime == 0){
                  this.Skill[5].removeFromParent();
            }
          }
        }
      }
      
  },
    shot: function(degree){
      if(this.dog.AIBOT == false){
         if(degree == "45"){
            if(this.Shooting == false){
                this.vx = 10;
                this.vy = 20;
            }
            else{
                this.vx += 0;
                this.vy += 0;
          }
      }else if(degree == "30"){
          if(this.Shooting == false){
              this.vx = 18;
              this.vy = 20;
          }
          else{
              this.vx += 0;
              this.vy += 0;
          }
      }else if(degree == "60"){
        
          if(this.Shooting == false){
              this.vx = 5;
              this.vy = 25;
          }
          else{
            this.vx += 0;
            this.vy += 0;
          }
      }
    }else{
        if(this.degree == 1 ){
            if(this.Shooting == false){
                this.vx = 10;
                this.vy = 20;
                this.degree = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
            }
            else {
                this.vx += 0;
                this.vy += 0;
            }
        }else if(this.degree == 2){
            if(this.Shooting == false){
                this.vx = 18;
                this.vy = 20;
                this.degree = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
            }
            else{
                this.vx += 0;
                this.vy += 0;
            }
      }else if(this.degree == 3){
            if(this.Shooting == false){
                this.vx = 5;
                this.vy = 25;
                this.degree = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
      }
      else{
         this.vx += 0;
         this.vy += 0;
      }
    }
    }
    },
    switchDirectionSTILL: function() {
      this.direction = itemdog1.DIR.STILL;
      }
});
itemdog2.G = -1;
itemdog2.STARTING_VELOCITY = 15;
itemdog2.DIR = {
    LEFT: 1,
    RIGHT: 2,
    SHOT: 3
};
