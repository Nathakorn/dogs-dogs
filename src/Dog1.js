  var Dog1 = cc.Sprite.extend({
    ctor: function() {
        this._super();
        this.initWithFile( 'images/dog1.png' );
    	  var animation = new cc.Animation.create();
        animation.addSpriteFrameWithFile( 'images/dog1runright1.png' );
        animation.addSpriteFrameWithFile( 'images/dog1runright2.png' );
        animation.setDelayPerUnit( 0.2 );
        var movingAction = cc.RepeatForever.create( cc.Animate.create( animation ) );
        this.runAction( movingAction );
        this.alive = true;
        this.vx = 3;
    },
    update: function( dt ) {
        var pos = this.getPosition();
        if(this.direction == Dog1.DIR.LEFT && this.alive == true){
            if( pos.x < 0 ) {
                this.setPosition( new cc.Point( pos.x + 1, pos.y  ) );
            }else{
                this.setPosition( new cc.Point( pos.x - this.vx, pos.y ) );
            }
        }else if (this.direction == Dog1.DIR.RIGHT && this.alive == true){
            if(pos.x > (screenWidth /2) - 50 ){
                this.setPosition( new cc.Point( pos.x - 1  , pos.y ) );
            }else{
                this.setPosition( new cc.Point( pos.x + this.vx , pos.y ) );
            }
        }
    },
    switchDirection: function(direction) {
        if( direction == "RIGHT" ) {
            this.direction = Dog1.DIR.RIGHT;
        }else if( direction == "LEFT")
            this.direction = Dog1.DIR.LEFT; 
    },
    switchDirectionSTILL: function() {
        this.direction = Dog1.DIR.STILL;
    }
});
Dog1.DIR = {
    LEFT: 1,
    RIGHT: 2
};