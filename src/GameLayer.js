var GameLayer = cc.Layer.extend({
    ctor: function(select){
        this._super();
        this.select = select;
    },
    init: function() {
        this.setPosition( new cc.Point( 0, 0 ) );
        this.setKeyboardEnabled( true );

        this.BackGround = new BackGround('images/background1.png' );
        this.BackGround.setPosition( new cc.Point( 497.5, 300 ) );
        this.addChild( this.BackGround );
         
        this.Skill=[];
        this.Skill.push(new Skill('images/moredamage2.png'));
        this.Skill.push(new Skill('images/speed2.png'));
        this.Skill.push(new Skill('images/plus.png'));
        this.Skill.push(new Skill('images/moredamage2.png'));
        this.Skill.push(new Skill('images/speed2.png'));
        this.Skill.push(new Skill('images/plus.png'));
        this.Skill[0].setPosition( new cc.Point( 70, 500 ) );
        this.Skill[1].setPosition( new cc.Point( 130, 500 ) );
        this.Skill[2].setPosition( new cc.Point( 190, 500 ) );
        this.Skill[3].setPosition( new cc.Point( 610, 500 ) );
        this.Skill[4].setPosition( new cc.Point( 670, 500 ) );
        this.Skill[5].setPosition( new cc.Point( 730, 500 ) );
        for(i = 0 ; i < 6; i++){
        this.addChild( this.Skill[i] );
        }
       
        this.Net = new Net();
        this.Net.setPosition( new cc.Point( 460, 130 ) );
        this.addChild(this.Net);
        
        this.Barhealth = new Barhealth();
        this.Barhealth.setPosition( new cc.Point( 350, 440 ) );
        this.addChild( this.Barhealth );
       
        this.Barhealth2 = new Barhealth2();
        this.Barhealth2.setPosition( new cc.Point( 760, 440 ) );
        this.addChild( this.Barhealth2 );
       
        this.itemdog1 = new itemdog1( this.Skill );
        this.itemdog1.setPosition( new cc.Point( 100, 75 ) );
        this.addChild( this.itemdog1  ,2);
        this.itemdog1.scheduleUpdate();

        this.Dog1 = new Dog1();
        this.Dog1.setPosition( new cc.Point( 110, 75 ) );
        this.addChild( this.Dog1 ,  4);
        this.Dog1.scheduleUpdate();
        
        this.itemdog2 = new itemdog2(this.Skill);
        this.itemdog2.setPosition( new cc.Point( 700, 75 ) );
        this.addChild( this.itemdog2 , 1);
        this.itemdog2.scheduleUpdate();

        this.Dog2 = new Dog2(this.select);
        this.Dog2.setPosition( new cc.Point( 700, 75 ) );
        this.addChild( this.Dog2 , 3);
        this.Dog2.scheduleUpdate();
        
        this.itemdog1.setDog(this.Dog1);
        this.itemdog1.setDog2(this.Dog2);
        this.itemdog1.setBarhealth1(this.Barhealth);
        this.itemdog1.setBarhealth2(this.Barhealth2);
        this.itemdog1.setItemDog2(this.itemdog2);
        
        this.itemdog2.setDog(this.Dog2);
        this.itemdog2.setDog1(this.Dog1);
        this.itemdog2.setBarhealth1(this.Barhealth);
        this.itemdog2.setBarhealth2(this.Barhealth2);
        this.itemdog2.setItemDog1(this.itemdog1);
        
        this.timeLabel = cc.LabelTTF.create( '60', 'Bauhaus 93', 40  );
        this.timeLabel.setPosition( new cc.Point(398, 540) );
        this.timeLabel.setFontFillColor( new cc.Color3B(0, 0, 255));
        this.addChild( this.timeLabel );
        this.schedule(this.times,1);

        this.timeoutLabel = cc.LabelTTF.create( '', 'Bauhaus 93', 100 );
        this.timeoutLabel.setPosition( new cc.Point(398, 300) );
        this.timeoutLabel.setFontFillColor( new cc.Color3B(255, 0, 0));
        this.addChild( this.timeoutLabel );
        this.setMouseEnabled(true);
        this.timesec = 59;
        this.timeoutdelay = 2;
        this.enterdelay = 4;
        var audioEngine = cc.AudioEngine.getInstance();     
        audioEngine.playMusic(s_music_background, true);

        return true;
    },
    onKeyDown: function( e ) {
        if ( e == cc.KEY.d ) {
            this.Dog1.switchDirection("RIGHT");
        }else if ( e == cc.KEY.a ){
            this.Dog1.switchDirection("LEFT");
        }else if ( e == cc.KEY.v){
            this.itemdog1.shot("45");
            this.itemdog1.switchDirection("SHOT");
        }else if ( e == cc.KEY.c){
            this.itemdog1.shot("30");
            this.itemdog1.switchDirection("SHOT");
        }else if ( e == cc.KEY.b){
           this.itemdog1.shot("60");
           this.itemdog1.switchDirection("SHOT");
        }else if ( e == cc.KEY.f){
           this.itemdog1.SkillItem(1);
        }else if ( e == cc.KEY.g){
           this.itemdog1.SkillItem(2);
        }else if ( e == cc.KEY.h){
           this.itemdog1.SkillItem(3);
        }
        if(this.Dog2.AIBOT == false){
            if( e == cc.KEY.right ) {
                this.Dog2.switchDirection("RIGHT");
            }else if( e == cc.KEY.left ){
                this.Dog2.switchDirection("LEFT");
            }else if ( e == 190){
                this.itemdog2.shot("45");
                this.itemdog2.switchDirection("SHOT");
            }else if ( e == 188){
                this.itemdog2.shot("30");
                this.itemdog2.switchDirection("SHOT");
            }else if ( e == 191){
                this.itemdog2.shot("60");
                this.itemdog2.switchDirection("SHOT");
            }else if ( e == 76){
                this.itemdog2.SkillItem(1);
            }else if ( e == 59){
                this.itemdog2.SkillItem(2);
            }else if ( e == 222){
                this.itemdog2.SkillItem(3);
            }
        }
        console.log( e ); // To know position keyboard but don't need in game.
        if(e == 13){
            this.itemdog1.dog2effect.stopAllEffects();
            this.itemdog2.dog1effect.stopAllEffects();
            var director = cc.Director.getInstance();
            director.replaceScene(cc.TransitionFade.create(1.5, new Startscene(this.select)));
        }
    },
    onMouseDown: function( e ){ // To know position point but don't need in game.
        var pos = e.getLocation();
        console.log('x: '+pos.x+' y: '+pos.y);
    },
    times: function(){
        var whistleEffect = cc.AudioEngine.getInstance();
        whistleEffect.preloadMusic(whistle);
        if(this.timesec >= 0 ){
            this.timeLabel.setString( this.timesec );
            this.timesec -= 1;
            if(this.itemdog1.win == true){
                this.timeoutLabel.setString( "player 1 win!!!" );
                this.timesec = 0;
                this.enterdelay -= 1;
                if(this.enterdelay <= 0){
                    this.timeoutLabel.setString( "Enter to restart!" );
                }
            }
            if(this.itemdog2.win == true){
                this.timeoutLabel.setString( "player 2 win!!!" );
                this.timesec = 0;
                this.enterdelay -= 1;
                if(this.enterdelay <= 0){
                    this.timeoutLabel.setString( "Enter to restart!" );
                }
            }
        }
        else{
            this.itemdog1.timeout =  true;
            this.itemdog2.timeout =  true;
            this.timeoutLabel.setString( "time's up!!" );
            this.timeoutdelay--;
            if(this.timeoutdelay <= 0){
                if(this.itemdog1.health > this.itemdog2.health ){
                    this.timeoutLabel.setString( "player 2 win!!!" );
                    this.enterdelay -= 1;
                    if(this.enterdelay <= 0){
                        this.timeoutLabel.setString( "Enter to restart!" );
                    } 
                }
                else if(this.itemdog1.health < this.itemdog2.health ){
                    this.timeoutLabel.setString( "player 1 win!!!" );
                    this.enterdelay -= 1;
                    if(this.enterdelay <= 0){
                        this.timeoutLabel.setString( "Enter to restart!" );
                    }
                }
                else{
                    this.timeoutLabel.setString( "draw!!!" );
                    this.enterdelay -= 1;
                    if(this.enterdelay <= 0){
                        this.timeoutLabel.setString( "Enter to restart!" );
                    }
                }
            }
        }
        if(this.timesec == 0 ){
            if(this.Dog1.alive == true && this.Dog2.alive == true){
                whistleEffect.playEffect( 'effects/Whistle.mp3' );
            }
        }
    },
});
var Startscene = cc.Scene.extend({
    ctor: function(select){
        this._super();
        this.select = select;
    },
    onEnter: function() {
        this._super();
        var layer = new GameLayer(this.select);
        layer.init();
        this.addChild( layer );
    }
});

